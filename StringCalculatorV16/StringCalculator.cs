﻿

using System;
using System.Collections.Generic;

namespace StringCalculatorV16
{
    public class StringCalculator
    {
        private char comma = ',';
        private char newLine = '\n';
        private char slash = '/';
        private char leftSquare = '[';
        private char rightSquare = ']';
        private string doubleSlash = "//";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var delimiters = GetDelimiters(numbers);
            var total = GetTotal(numbers, delimiters);

            return total;
        }
        
        private List<string> GetDelimiters(string numbers)
        {
            var delimiters =new List<string>(new string[] { comma.ToString(), newLine.ToString() }) ;

            if (numbers.StartsWith(doubleSlash))
            {
                numbers = string.Concat(numbers.Split(slash));

                var delimiter = numbers.Substring(0, numbers.IndexOf(newLine));

                delimiters.Clear();
                delimiters.Add(delimiter);
            }
            
            if (numbers.Contains(leftSquare.ToString()))
            {
                numbers = string.Concat(numbers.Split(slash));

                var delimiterArray = numbers.Substring(0, numbers.IndexOf(newLine)).Split(leftSquare,rightSquare);

                delimiters.Clear();

                foreach (var delimiter in delimiterArray)
                {
                    delimiters.Add(delimiter);
                }
            }
            
            return delimiters;
        }

        private int GetTotal(string numbers, List<string> delimiters)
        {
            var sum = 0;
            var negativeNumbers = string.Empty;

            if (numbers.StartsWith(slash.ToString()))
            {
                numbers = string.Concat(numbers.Split(slash,newLine,leftSquare,rightSquare));
            }

            foreach (var number in numbers.Split(delimiters.ToArray(),StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumbers = int.Parse(number);

                    if (convertedNumbers < 0)
                    {
                        negativeNumbers = string.Join(" ", negativeNumbers, number);
                    }

                    if (convertedNumbers <= 1000)
                    {
                        sum += convertedNumbers;
                    }
                }
                catch
                {
                    throw new Exception("Invalid delimiters");
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                numbers = string.Concat(numbers.Split(slash));
                throw new Exception("Negatives not allowed." + negativeNumbers);
            }
            return sum;
        }

    }
}
