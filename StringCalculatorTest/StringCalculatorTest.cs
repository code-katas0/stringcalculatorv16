﻿using NUnit.Framework;
using StringCalculatorV16;
using System;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();

        }
        
        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 9;

            //Act
            var actual = _calculator.Add("1,2,3,3");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 9;

            //Act
            var actual = _calculator.Add("1\n2,3,3");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenCustomDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 8;

            //Act
            var actual = _calculator.Add("//;\n2;3;3");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void GivenAnyLengthDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 8;

            //Act
            var actual = _calculator.Add("//***\n2***3***3");

            //Assert
            Assert.AreEqual(expected, actual);

        }
        
        [Test]
        public void GivenNegativenumbers_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Negatives not allowed. -3 -3";

            //Act
            var exception =Assert.Throws<Exception>(()=> _calculator.Add("//;\n-3;-3"));

            //Assert
            Assert.AreEqual(expected, exception.Message);

        }

        [Test]
        public void GivenNumbers_WhenAdding_ThenIgnoreNumbersGreaterThousand()
        {
            //Arrange
            var expected = 8;

            //Act
            var actual = _calculator.Add("//;\n2;3;3;1001");

            //Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GivenMultipleDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 8;

            //Act
            var actual = _calculator.Add("//[*][&]\n2*3&3");

            //Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GivenInvalidDelimiters_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Invalid delimiters";

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n-3;%%-3"));

            //Assert
            Assert.AreEqual(expected, exception.Message);

        }

        [Test]
        public void GivenUnlimitedMultipleDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 8;

            //Act
            var actual = _calculator.Add("//[*$$][&##]\n2*$$3&##3");

            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}
